package com.fiap.api.repository;

import com.fiap.api.model.Setup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface SetupRepository extends JpaRepository<Setup, Long> {}

